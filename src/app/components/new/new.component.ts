import { Component, Input, OnInit } from '@angular/core';
import { NewModel } from '../../models/new.model';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  @Input() new: NewModel;
  @Input() isDetail?: boolean;

  public newsText: string;

  constructor() { }

  ngOnInit(): void {
    if (this.isDetail) {
      this.newsText = this.new.text;
    } else {
      const tempText = this.new.text.split('.');
      this.newsText = tempText[0];
    }
  }

}
