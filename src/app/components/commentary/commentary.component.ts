import {Component, Input, OnInit} from '@angular/core';
import {NewsMock} from '../../mocks/news.mock';
import {Form} from '@angular/forms';
import {CommentModel} from '../../models/comment.model';
import {ActivatedRoute} from '@angular/router';
import {NewModel} from '../../models/new.model';

@Component({
  selector: 'app-commentary',
  templateUrl: './commentary.component.html',
  styleUrls: ['./commentary.component.scss']
})
export class CommentaryComponent implements OnInit {

  @Input() name: string;
  @Input() timestamp: number;
  @Input() text: string;
  @Input() answers: any[];

  public user = JSON.parse(localStorage.getItem('user'));
  public newDetail: NewModel;

  public textEdit: boolean = false;

  constructor(public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.newDetail = NewsMock.find(item => item.id == params.id);
    });
  }

  editComment(): void{
    this.textEdit = true;
  }

  submitAnswer(event: Event, form: any, text: string, timestamp: number): void {
    this.textEdit = false;
    const activeComment = this.getActiveComment(text, timestamp);
    activeComment.text = event.target[0].value;
    form.reset();
  }

  cancelAction(form: any): void {
    this.textEdit = false;
    form.reset();
  }

  deleteComment(text: string, timestamp: number): void {
    const activeComment = this.getActiveComment(text, timestamp);
    const activeIndex = this.newDetail.comments.indexOf(activeComment);
    this.newDetail.comments.splice(activeIndex, 1);
  }

  getActiveComment(text: string, timestamp: number): CommentModel {
    return this.newDetail.comments.find(item => item.text === text && item.timestamp === timestamp);
  }
}
