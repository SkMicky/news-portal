import { Component, OnInit } from '@angular/core';
import {NewsService} from '../../services/news.service';
import {NewModel} from '../../models/new.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  public news: NewModel[] = [];
  public loading: boolean;

  public pageNumber: number = 1;
  public pageButtons: number[];

  constructor(public newsService: NewsService) { }

  ngOnInit(): void {
    this.newsService.searchValue.subscribe(data => {
      this.pageButtons = [];
      this.getNews(this.pageNumber);
      for (let i = 1; i <= this.newsService.totalPages; i++) {
        this.pageButtons.push(i);
      }
    });
  }

  goNextPage(): void {
    this.pageNumber += 1;
    this.getNews(this.pageNumber);
  }

  goPreviousPage(): void {
    this.pageNumber -= 1;
    this.getNews(this.pageNumber);
  }

  goFirstPage(): void {
    this.pageNumber = 1;
    this.getNews(this.pageNumber);
  }

  goLastPage(): void {
    this.pageNumber = this.newsService.totalPages;
    this.getNews(this.pageNumber);
  }

  public getNews(pageNumber: number): void {
    this.loading = true;
    this.pageNumber = pageNumber;
    this.news = [];
    setTimeout(() => {
      this.news = [...this.newsService.news].splice((pageNumber - 1) * 10, 10);
      this.loading = false;
    }, 500);
  }

}
