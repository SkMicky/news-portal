import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginFormModel} from '../../models/login-form.model';
import {UsersMock} from '../../mocks/users.mock';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  @Output() cancelAuth: EventEmitter<any> = new EventEmitter<any>();

  public loginForm: FormGroup;
  public isIncorrectUser: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  closeForm(): void {
    this.cancelAuth.emit();
  }

  doLogin(value: LoginFormModel): void {
    const person = UsersMock.find(item => item.login === value.login && item.password === value.password);
    this.isIncorrectUser = !person;
    if (person) {
      localStorage.setItem('user', JSON.stringify(person));
      window.location.reload();
    }

    this.closeForm();
  }
}
