import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegistrationFormModel} from '../../models/registration-form.model';
import {UsersMock} from '../../mocks/users.mock';
import {UserModel} from '../../models/user.model';
import {Role} from '../../enum/role.enum';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister: EventEmitter<any> = new EventEmitter<any>();

  public registerForm: FormGroup;

  constructor() { }

  ngOnInit(): void {
    const namePattern = '^[a-zA-Zа-яА-Я]+$';
    const phoneNumberPattern = '^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$';
    this.registerForm = new FormGroup({
      lastName: new FormControl('', [Validators.required, Validators.pattern(namePattern)]),
      firstName: new FormControl('', [Validators.required, Validators.pattern(namePattern)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, Validators.pattern(phoneNumberPattern)]),
      login: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  closeForm(): void {
    this.cancelRegister.emit();
  }

  doRegister(value: RegistrationFormModel): void {
    const user: UserModel = {
      email: value.email,
      firstName: value.firstName,
      lastName: value.lastName,
      login: value.login,
      password: value.password,
      phone: value.phone,
      role: Role.USER
    };
    UsersMock.push(user);
    this.closeForm();
  }

}
