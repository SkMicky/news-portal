import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserModel} from '../../models/user.model';
import {NewsService} from '../../services/news.service';
import {NewsMock} from '../../mocks/news.mock';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() isNeedSearch: boolean;
  @Output() openLogin: EventEmitter<any> = new EventEmitter<any>();
  @Output() openRegister: EventEmitter<any> = new EventEmitter<any>();

  public profileClicked: boolean = false;
  public user: UserModel;

  constructor(public newsService: NewsService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  loginClick(): void {
    this.openLogin.emit();
  }

  signUpClick(): void {
    this.openRegister.emit();
  }

  onSearch(searchString: string): void {
    if (searchString === '') {
      this.newsService.news = NewsMock;
      this.newsService.totalPages = this.newsService.news.length / 10;
      this.newsService.searchValue.next(false);
      return;
    }
    this.newsService.news = NewsMock;
    this.newsService.news = [...this.newsService.news].filter(item => item.title.includes(searchString));
    this.newsService.totalPages = this.newsService.news.length / 10;
    this.newsService.searchValue.next(true);
  }

  showLogout(): void {
    this.profileClicked = !this.profileClicked;
  }

  logout(): void {
    this.user = undefined;
    localStorage.clear();
    window.location.reload();
  }
}
