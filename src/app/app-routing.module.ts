import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {NewsDetailComponent} from './pages/news-detail/news-detail.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'one/:id', component: NewsDetailComponent },
  { path: '**', component: MainComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
