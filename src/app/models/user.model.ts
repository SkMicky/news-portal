import {Role} from '../enum/role.enum';

export class UserModel {
  email: string;
  firstName: string;
  lastName: string;
  login: string;
  password: string;
  phone: string;
  role: Role;
}
