export interface CommentModel {
  author: string;
  timestamp: number;
  text: string;
}
