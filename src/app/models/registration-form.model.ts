export class RegistrationFormModel {
  email: string;
  firstName: string;
  lastName: string;
  login: string;
  password: string;
  phone: string;
}
