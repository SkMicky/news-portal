import { CommentModel } from './comment.model';

export class NewModel {
  id: number;
  heading: string;
  title: string;
  author: string;
  timestamp: number;
  image?: string;
  text: string;
  comments: CommentModel[];
}
