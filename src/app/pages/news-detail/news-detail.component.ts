import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NewModel} from '../../models/new.model';
import {NewsMock} from '../../mocks/news.mock';
import {UserModel} from '../../models/user.model';
import {Form} from '@angular/forms';
import {CommentModel} from '../../models/comment.model';

@Component({
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit {
  public openLoginForm = false;
  public openRegisterForm = false;
  public newDetail: NewModel;
  public user: UserModel;

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.route.params.subscribe(params => {
      this.newDetail = NewsMock.find(item => item.id == params.id);
    });
  }

  openLoginModal(): void {
    this.openRegisterForm = false;
    this.openLoginForm = true;
  }

  openRegisterModal(): void {
    this.openLoginForm = false;
    this.openRegisterForm = true;
  }

  closeModals(): void {
    this.openLoginForm = false;
    this.openRegisterForm = false;
  }

  submitComment(event: Event, form: any): void {
    const author = this.user.lastName + ' ' + this.user.firstName;

    const comment: CommentModel = {
      author,
      timestamp: new Date().getTime(),
      text: event.target[0].value,
    };

    this.newDetail.comments.push(comment);
    form.reset();
  }

  cancelSubmit(form: any): void {
    form.reset();
  }

}
