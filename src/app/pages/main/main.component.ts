import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public openLoginForm = false;
  public openRegisterForm = false;

  constructor() { }

  ngOnInit(): void {}

  openLoginModal(): void {
    this.openRegisterForm = false;
    this.openLoginForm = true;
  }

  openRegisterModal(): void {
    this.openLoginForm = false;
    this.openRegisterForm = true;
  }

  closeModals(): void {
    this.openLoginForm = false;
    this.openRegisterForm = false;
  }
}
