import { Injectable } from '@angular/core';
import { NewModel } from '../models/new.model';
import { NewsMock } from '../mocks/news.mock';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  public news: NewModel[] = NewsMock;
  public totalPages = this.news.length / 10;
  public searchValue: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {}
}
