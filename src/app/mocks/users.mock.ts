import { UserModel } from '../models/user.model';
import { Role } from '../enum/role.enum';

export const UsersMock: UserModel[] = [
  {
    email: 'admin@test.kz',
    firstName: 'Иванов',
    lastName: 'Иван',
    login: 'admin@test.kz',
    password: 'admin',
    phone: '84953231452',
    role: Role.ADMIN,
  },
  {
    email: 'user@test.kz',
    firstName: 'Беспалов',
    lastName: 'Андрей',
    login: 'user@test.kz',
    password: 'user',
    phone: '87757231378',
    role: Role.USER,
  },
];
